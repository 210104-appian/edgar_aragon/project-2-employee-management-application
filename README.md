# Employee Management Application

## Project Description
An Appian application with some functionality to assist employee management. Includes lots of additional and unnecessary objects that were created for the purposes of learning Appian.

## Technologies used
- Appian 20.4

## Features
--------------------------------------------------------------
- Working Process model, with Write to Datastore Entity and Write to Multiple Datastore Entity.
- Employee data, secondary and address data being commited.
- Expression rules fetching employees.
- Paging and editable grids displaying info and enabling updates.
- Functioning records,sites,tasks, tempo and reports.

## Getting Started
- Requires being created as a user for the revature appian cloud development environment.
- Log in using credentials given by administrator.
- To work in tempo select Actions and click on EEM Add New Employee to add a new employee.
- Click on records and select EEM Records to view a grid view of all the employees in the system.
- Can filter employees via search, salary, and the date the employee is created on.
- View the Employee Drill Down report by clicking on Reports and selecting EEM Employee Status Report
- Can view Approved, Pending and Rejected employees by clicking on the respective area of the pie chart
- Can get a similar experience using the site, by clicking on the navigation bar and selecting EEM HR Site

## Usage
This application was created for learning purposes only. 
Large portions of this application were used as a sand box for testing different functionality.
in Appian and learning how to use different features.
